﻿namespace course.api.Models
{
    /// <summary>
    /// Model CursoViewModelInput
    /// </summary>
    public class CursoViewModelInput
    {
        /// <summary>
        /// Prop Nome
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Prop Descricao
        /// </summary>
        public string Descricao { get; set; }
    }
}